# VALIDA SIMULATION
Este proyecto se ha creado para simular el circuito RTL para le proyecto VALIDA. 

El simulador utilizado es el MODELSIM, por lo que muchos de los ficheros que están aquí subidos (algunas extensiones como `.mpf`, `.mti`, etc) son del propio simulador. 

El fichero `testbench` es el `tb_main.vhd`, el cual testea el fichero [src/AFE.vhd](src/AFE.vhd). 
El testbench se encarga de leer un fichero `.txt` que se encuentra en la carpeta `Input`. Asi mismo guarda los resultados en un fichero `.csv`. 
Esto lo podemos ver definido en los generic del `testbench` : 

```vhdl
generic (
        G_FILE_INPUT_STR : STRING := "Input/sound_cat.txt";
        G_FILE_OUTPUT_STR : STRING := "Output/sound_cat_results.csv"
    );
```

# BATCH SCRIPT

También se agrega una carpeta llamada `batch_script`, donde se puede correr la simulación desde la consola, sin necesidad de abrir la interfaz grafica del  `MODELSIM`. 


