onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_afe/clk
add wave -noupdate /tb_afe/i_nreset
add wave -noupdate /tb_afe/i_enable
add wave -noupdate -radix unsigned /tb_afe/index_csv_i
add wave -noupdate -radix decimal /tb_afe/i_senyal_u
add wave -noupdate /tb_afe/o_out_enable
add wave -noupdate -childformat {{/tb_afe/o_weights(32) -radix decimal} {/tb_afe/o_weights(31) -radix decimal} {/tb_afe/o_weights(30) -radix decimal} {/tb_afe/o_weights(29) -radix decimal} {/tb_afe/o_weights(28) -radix decimal} {/tb_afe/o_weights(27) -radix decimal} {/tb_afe/o_weights(26) -radix decimal} {/tb_afe/o_weights(25) -radix decimal} {/tb_afe/o_weights(24) -radix decimal} {/tb_afe/o_weights(23) -radix decimal} {/tb_afe/o_weights(22) -radix decimal} {/tb_afe/o_weights(21) -radix decimal} {/tb_afe/o_weights(20) -radix decimal} {/tb_afe/o_weights(19) -radix decimal} {/tb_afe/o_weights(18) -radix decimal} {/tb_afe/o_weights(17) -radix decimal} {/tb_afe/o_weights(16) -radix decimal} {/tb_afe/o_weights(15) -radix decimal} {/tb_afe/o_weights(14) -radix decimal} {/tb_afe/o_weights(13) -radix decimal} {/tb_afe/o_weights(12) -radix decimal} {/tb_afe/o_weights(11) -radix decimal} {/tb_afe/o_weights(10) -radix decimal} {/tb_afe/o_weights(9) -radix decimal} {/tb_afe/o_weights(8) -radix decimal} {/tb_afe/o_weights(7) -radix decimal} {/tb_afe/o_weights(6) -radix decimal} {/tb_afe/o_weights(5) -radix decimal} {/tb_afe/o_weights(4) -radix decimal} {/tb_afe/o_weights(3) -radix decimal} {/tb_afe/o_weights(2) -radix decimal} {/tb_afe/o_weights(1) -radix decimal}} -subitemconfig {/tb_afe/o_weights(32) {-height 15 -radix decimal} /tb_afe/o_weights(31) {-height 15 -radix decimal} /tb_afe/o_weights(30) {-height 15 -radix decimal} /tb_afe/o_weights(29) {-height 15 -radix decimal} /tb_afe/o_weights(28) {-height 15 -radix decimal} /tb_afe/o_weights(27) {-height 15 -radix decimal} /tb_afe/o_weights(26) {-height 15 -radix decimal} /tb_afe/o_weights(25) {-height 15 -radix decimal} /tb_afe/o_weights(24) {-height 15 -radix decimal} /tb_afe/o_weights(23) {-height 15 -radix decimal} /tb_afe/o_weights(22) {-height 15 -radix decimal} /tb_afe/o_weights(21) {-height 15 -radix decimal} /tb_afe/o_weights(20) {-height 15 -radix decimal} /tb_afe/o_weights(19) {-height 15 -radix decimal} /tb_afe/o_weights(18) {-height 15 -radix decimal} /tb_afe/o_weights(17) {-height 15 -radix decimal} /tb_afe/o_weights(16) {-height 15 -radix decimal} /tb_afe/o_weights(15) {-height 15 -radix decimal} /tb_afe/o_weights(14) {-height 15 -radix decimal} /tb_afe/o_weights(13) {-height 15 -radix decimal} /tb_afe/o_weights(12) {-height 15 -radix decimal} /tb_afe/o_weights(11) {-height 15 -radix decimal} /tb_afe/o_weights(10) {-height 15 -radix decimal} /tb_afe/o_weights(9) {-height 15 -radix decimal} /tb_afe/o_weights(8) {-height 15 -radix decimal} /tb_afe/o_weights(7) {-height 15 -radix decimal} /tb_afe/o_weights(6) {-height 15 -radix decimal} /tb_afe/o_weights(5) {-height 15 -radix decimal} /tb_afe/o_weights(4) {-height 15 -radix decimal} /tb_afe/o_weights(3) {-height 15 -radix decimal} /tb_afe/o_weights(2) {-height 15 -radix decimal} /tb_afe/o_weights(1) {-height 15 -radix decimal}} /tb_afe/o_weights
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {40000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1000
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {4838617 ps} {5518493 ps}
