echo running simulation...
onerror { exit }
vsim -novopt -quiet -GG_FILE_INPUT_STR="../Input/sound_bird.txt" -GG_FILE_OUTPUT_STR="../Output/sound_bird_result.csv" ../work.tb_afe;
run -all;
quit -sim;
exit;
