-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 25.7.2022 13:29:26 UTC

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

use STD.textio.all;
use ieee.std_logic_textio.all;

USE WORK.mytypes.ALL; --Auxiliar file for configuration purporses

-- GG_FILE_INPUT_STR="Input/sound_cat.txt"

entity tb_AFE is

    generic (
        G_FILE_INPUT_STR : STRING := "Input/sound_cat.txt";
        G_FILE_OUTPUT_STR : STRING := "Output/sound_cat_results.csv"
    );

end tb_AFE;

architecture tb of tb_AFE is

    component AFE
        port (clk          : in std_logic;
              i_nreset     : in std_logic;
              i_enable     : in std_logic;
              i_senyal_u   : in signed ((rc_precision -1) downto 0);
              o_out_enable : out std_logic;
              o_weights    : out unarray_plus ((n+1) downto 1));
    end component;

    signal clk          : std_logic;
    signal i_nreset     : std_logic;
    signal i_enable     : std_logic;
    signal i_senyal_u   : signed ((rc_precision -1) downto 0) := (others=>'0');
    signal o_out_enable : std_logic;
    signal o_weights    : unarray_plus ((n+1) downto 1);

    constant TbPeriod : time := 20 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

    -- constant AUDIO_LEN : integer := 16000;
    -- signal cont_clk : integer range 0 to AUDIO_LEN-1;

    -- csv
    file file_input : text ;
    file file_output : text;

    signal index_csv_i : integer;

begin

    dut : AFE
    port map (clk          => clk,
              i_nreset     => i_nreset,
              i_enable     => i_enable,
              i_senyal_u   => i_senyal_u,
              o_out_enable => o_out_enable,
              o_weights    => o_weights);


    -- rom_inst: entity work.rom_audio    
    -- port map (cont_clk, i_senyal_u);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;




    -- proceso para enviar datos desde csv
    read_csv: process
        variable v_ILINE            : line;
        variable v_oline_output     : line;
        variable v_tmpInt           : integer;
        variable i                  : integer;        
        variable tmpchar            : character;
    begin
        ----------------------------------
        -- Read the input
        ----------------------------------
        file_open(file_input, G_FILE_INPUT_STR, read_mode);

        wait for 20ns;
        wait until i_nreset = '1';

        -- wait for first 2 pulses on o_out_enable to stability
        -- wait until o_out_enable = '1';
        -- wait until o_out_enable = '0';
 

        index_csv_i <= 0;
        -- while not endfile(file_input) loop   
        while True loop 
            readline(file_input, v_ILINE);
            read(v_ILINE, v_tmpInt);
            if v_ILINE'length > 0 then-- le coma a menos que sea el ultimo campo
                read(v_ILINE, tmpchar);-- lee coma            
            end if;
            i_senyal_u <= to_signed(v_tmpInt, RC_PRECISION);

            wait for TbPeriod;
            index_csv_i <= index_csv_i + 1;
        end loop;

    end process;


    -- proceso para guardar resultados en csv
    write_csv: process
        variable v_ILINE            : line;
        variable v_oline_output     : line;
        variable v_tmpInt           : integer;
        variable i                  : integer;        
        variable tmpchar            : character;
    begin
        file_open(file_output, G_FILE_OUTPUT_STR, write_mode);
        
        -- first pulse dummy
        wait until o_out_enable = '1';
        wait until o_out_enable = '0';

        ----------------------------------
        -- save .csv
        ----------------------------------
        while True loop
            wait until o_out_enable = '1';
            wait until o_out_enable = '0';

            i := 1;           
            while i < (N+1) loop 
                -- write output.csv
                write(v_oline_output, to_integer(o_weights(i)));
                write(v_oline_output, ',');
                i := i+1;           
            end loop;
            write(v_oline_output, to_integer(o_weights(i)));        
            writeline(file_output, v_oline_output);
        end loop;
    end process;



    stimuli : process        
    begin
        -- EDIT Adapt initialization as needed
        i_enable <= '1';        

        -- Reset generation
        i_nreset <= '0';
        wait for 40 ns;
        i_nreset <= '1';
        
        wait;
    end process;

end tb;

