-- Josep L. Rossello. Universitat Illes Balears. Mar 2022.

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.all;

PACKAGE mytypes IS

-- Definitions for the reservoir topology
CONSTANT RC_PRECISION: integer:=8; -- Precision of the reservoir
CONSTANT N: integer:=31; -- Number of neurons. Normally the system is created so that N=M for simplicity so we fix as many neurons as inputs
CONSTANT L: integer:=128; -- Sample length

-- Definition of arrays with arbitrary length of BIT_PRECISSION, BIT_PRECISSION+1, and double precission (unarray, unarray_plus and unarray_double)
TYPE unarray_simple IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION-1 DOWNTO 0);
TYPE unarray_plus IS ARRAY (NATURAL RANGE <>) OF SIGNED (RC_PRECISION DOWNTO 0);

END mytypes;

