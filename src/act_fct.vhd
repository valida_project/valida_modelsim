LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE work.mytypes.ALL; --Auxiliar file for configuration purporses


---------------------------------------------------------------------------------------------------------------------------
-- Piece-wise linear limiting function to [-1,1] (1 is defined as the maximum range available with RC_PRECISION)
-- f(x)=-1 if x<-1
-- f(x)=x if -1<x<+1
-- f(x)=+1 if x>1
---------------------------------------------------------------------------------------------------------------------------

ENTITY act_fct IS
PORT ( i_x: IN SIGNED (RC_PRECISION DOWNTO 0);
		o_f: OUT SIGNED ((RC_PRECISION-1) DOWNTO 0));
END act_fct;

ARCHITECTURE funcio OF act_fct IS

BEGIN 
	o_f<= to_signed(2**(RC_PRECISION-1)-1,o_f'LENGTH) WHEN (i_x> 2**(RC_PRECISION-1)-1) ELSE
		to_signed(-2**(RC_PRECISION-1),o_f'LENGTH) WHEN (i_x< -2**(RC_PRECISION-1)) ELSE
		resize(i_x,RC_PRECISION); 
		
END funcio;