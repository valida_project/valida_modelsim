-- Design Unit: SOUND RECOGNITION SYSTEM BASED ON RESERVOIR COMPUTING
-- Purpose: VALENTRA PROJECT 
-- Clock Source: 50MHz 
-- Authors: J.L.Rossello (07/05/2019)
------------------------------------------------------------------------------------------------------------------------------
-- Version		Authors			Date				Changes
-- 0.1			J.L. Rossello  		07/05/2019    			First 16bits Version
-- 0.2			J.L. Rossello  		14/06/2019     		Differentiate RC_PRECISION=8b (Reservoir) and BIT_PRECISION for output layer
-- 0.3			J.L. Rossello  		19/06/2019				Automatic generation
-- 0.4			J.L. Rossello 			15/07/2021				Simplified one-dimensional input version
-- 0.5         J.L. Rossello        22/07/2022				Adapted to a 31 neuron reservoir
--========================================================================================================================

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.mytypes.all; --Auxiliar file for configuration purporses

ENTITY Reservoir IS
PORT ( 
		clk, i_nreset,i_enable: IN STD_LOGIC;
		i_senyal_u: IN SIGNED ((RC_PRECISION-1) DOWNTO 0);
		o_x_rc : OUT UNARRAY_SIMPLE(N DOWNTO 1));
END ENTITY Reservoir;

ARCHITECTURE net OF reservoir IS

COMPONENT ffD IS
PORT ( 		
	   clk, i_nreset,i_enable: IN STD_LOGIC; 		
		i_senyal_u: IN SIGNED ((RC_PRECISION-1) DOWNTO 0);
		o_qout: OUT SIGNED ((RC_PRECISION-1) DOWNTO 0));
END COMPONENT;

COMPONENT act_fct IS
PORT ( i_x: IN SIGNED (RC_PRECISION DOWNTO 0);
		o_f: OUT SIGNED ((RC_PRECISION-1) DOWNTO 0));
END COMPONENT;

SIGNAL prod1,prod2,out_x,out_x_aux : UNARRAY_SIMPLE(N DOWNTO 1);
SIGNAL sum: UNARRAY_PLUS(N DOWNTO 1);
SIGNAL signo: STD_LOGIC_VECTOR(N DOWNTO 1); -- signo defines the connectivity matrix between inputs and reservoir. 

BEGIN

--signo<=X"140b275c105b51e8";--Reservoir connectivity code. Caso de 64 neuronas
--signo<=X"105b51e8";--Reservoir connectivity code. Caso de 32 neuronas
signo<="0010000010110110101000111101000";
-- 1st neuron  
-- External product
prod1(1) <= i_senyal_u WHEN signo(1)='1' ELSE -i_senyal_u;--ACTIVAR-LA EN MODE SUPERIOR

-- Internal product
prod2(1) <= shift_right(out_x(N),1); -- Internal hyperparameter fixed to r=0.5 in this line
-- Sum of the two previous terms. Increase one bit to avoid overflow
sum(1) <= resize(prod1(1),RC_PRECISION+1)+resize(prod2(1),RC_PRECISION+1); 
f_tanh1: act_fct PORT MAP(sum(1),out_x_aux(1)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux
ff1: ffD PORT MAP(clk, i_nreset, i_enable, out_x_aux(1),out_x(1)); -- Force a delay of one clock cycle to each neuron

-- Rest of the reservoir (N-1 neurons)

reservorio: FOR i IN 2 TO N GENERATE
	prod1(i) <= i_senyal_u WHEN signo(i)='1' ELSE -i_senyal_u;
	prod2(i) <= shift_right(out_x(i-1),1); -- Internal hyperparameter r=0.5
	sum(i) <= resize(prod1(i),RC_PRECISION+1)+resize(prod2(i),RC_PRECISION+1);
	f_tanhi: act_fct PORT MAP(sum(i),out_x_aux(i)); -- put the result to the piece-wise activation function act_fct, you obtain out_x_aux
	ffi: ffD PORT MAP(clk, i_nreset, i_enable, out_x_aux(i),out_x(i)); -- Force a delay of one clock cycle to each neuron
END GENERATE;

o_x_rc<=out_x;
END net;
