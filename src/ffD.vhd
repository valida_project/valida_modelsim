LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_SIGNED.ALL;
USE IEEE.NUMERIC_STD.ALL;

USE work.mytypes.all;


ENTITY ffD IS
PORT ( 		
	   clk, i_nreset,i_enable: IN STD_LOGIC; 		
		i_senyal_u: IN SIGNED ((RC_PRECISION-1) DOWNTO 0); 
		o_qout: OUT SIGNED ((RC_PRECISION-1) DOWNTO 0));
END ffD;

ARCHITECTURE arch OF ffD IS

SIGNAL sortida: SIGNED ((RC_PRECISION-1) DOWNTO 0);

BEGIN

	PROCESS (clk)
	BEGIN
		IF (rising_edge(clk)) THEN
			IF i_nreset = '0' THEN 
				sortida <=  to_signed(0, sortida'length);
			ELSIF i_enable='1' THEN
				sortida <= i_senyal_u;
			END IF;
		END IF;
	END PROCESS;
	
	o_qout <= sortida;
	
END arch;